LOCUS       AB000126                1728 bp    DNA     linear   BCT 26-MAY-1999
DEFINITION  Klebsiella pneumoniae DNA for mannosyl transferase,
            phosphoribosyl-ATP pyrophosphohydrolase, complete cds.
ACCESSION   AB000126
VERSION     AB000126.1
KEYWORDS    phosphoribosyl-ATP pyrophosphohydrolase; phosphoribosyl-AMP
            cyclohydrolase; mannosyl transferase.
SOURCE      Klebsiella pneumoniae
  ORGANISM  Klebsiella pneumoniae
            Bacteria; Proteobacteria; Gammaproteobacteria; Enterobacterales;
            Enterobacteriaceae; Klebsiella.
REFERENCE   1
  AUTHORS   Sugiyama,T., Kido,N., Kato,Y., Koide,N., Yoshida,T. and Yokochi,T.
  TITLE     Evolutionary relationship among rfb gene clusters synthesizing
            mannose homopolymer as O-specific polysaccharides in Escherichia
            coli and Klebsiella
  JOURNAL   Gene 198 (1-2), 111-113 (1997)
   PUBMED   9370271
REFERENCE   2  (bases 1 to 1728)
  AUTHORS   Sugiyama,T.
  TITLE     Direct Submission
  JOURNAL   Submitted (27-DEC-1996) Tsuyoshi Sugiyama, Aichi Medical University
            Aichi Medical University, Department of Microbiology and Immunology
            Department of Microbiology and Immunology; Yazako Yazako, Nagakute,
            Aichi 480-11, Japan (Tel:0561-62-3311(ex.2271), Fax:0561-63-9187)
FEATURES             Location/Qualifiers
     source          1..1728
                     /organism="Klebsiella pneumoniae"
                     /mol_type="genomic DNA"
                     /strain="K53 serotype O3"
                     /db_xref="taxon:573"
     gene            159..983
                     /gene="wbdC"
     CDS             159..983
                     /gene="wbdC"
                     /codon_start=1
                     /transl_table=11
                     /product="mannosyl transferase"
                     /protein_id="BAA23228.1"
                     /translation="MDMLHLSARPDARTVVTYHSDIVKQKRLMKLYQPLQERFLASVD
                     CIVASSPNYVASSQTLKKYQDKTVVIPFGLEQHDVQHDPQRVAHWRETVGDNFFLFVG
                     AFRYYKGLHILLDAAERSRLPVVIVGGGPLEAEVRREAQQRGLSNVVFTGMLNDEDKY
                     ILFQLCRGVVFPSHLRSEAFGITLLEGARFARPLISCEIGTGTSFINQDKVNGCVIPP
                     NDSQALVEAMNELWHNDETASRYGENSRRRFEEMFTADHMIDAYVNLYTTLLESKS"
     regulatory      992..1014
                     /regulatory_class="terminator"
     gene            complement(1022..1618)
                     /gene="hisI"
     CDS             complement(1022..1618)
                     /gene="hisI"
                     /EC_number="3.5.4.19"
                     /note="phosphoribosyl-AMP cyclohydrolase"
                     /codon_start=1
                     /transl_table=11
                     /product="phosphoribosyl-ATP pyrophosphohydrolase"
                     /protein_id="BAA23229.1"
                     /translation="MLTEQLDWEKTDGMMPAIVQHAVSGEVLMLGYMNKEALEKTEAT
                     GKVTFYSRTKQRLWTKGETSGNVLNVVSITPDCDNDTLLVLVNPIGPTCHKGTTSCFG
                     ETGHQWLFLYQLEQLLAERKHADPESSYTAKLYASGTKRIAQKVGEEGVETALAATVN
                     DRFELKNEASDLMYHLLVLLQDQGLDLGEVIDNLKSRH"
ORIGIN      
        1 aagagacagg acctgtcgcc tacgaagacc atcgggtcat ttataataag cagctttttg
       61 aaattgcctc cacgccgttt tcgttgaaag cgttaaagcg ttttaagcag attaaagatg
      121 attacgacat catcaactac cattttccgt ttccattcat ggatatgttg catctctcgg
      181 cgcggcctga cgccagaacg gtggtgacct atcactcgga tattgtgaaa caaaaacggt
      241 taatgaagtt gtaccagccg ctgcaggagc gattcctcgc cagcgtagac tgcatcgtcg
      301 cctcgtcgcc caactacgtg gcctccagcc agaccctgaa aaaatatcag gataaaaccg
      361 tggtgatccc gtttggtctg gagcagcatg acgtgcagca cgatccgcag cgggtggcgc
      421 actggcggga aaccgtcggc gataacttct tcctcttcgt cggcgctttc cgctactaca
      481 aagggctgca cattctgctg gatgccgccg agcgtagccg gctgccggtg gtgatcgtcg
      541 ggggcgggcc gctggaggcg gaagtgcggc gtgaggcgca gcaacgcggg ctgagcaatg
      601 tggtgtttac cggcatgctc aacgacgaag ataaatacat tctcttccag ctctgccggg
      661 gcgtggtctt cccctcgcat ctgcgctccg aggcgtttgg cattacgtta ctggaaggcg
      721 cgcgcttcgc caggccgctg atctcttgcg agatcggcac cgggacctcg ttcattaacc
      781 aggacaaagt gaatggctgt gtgatcccgc cgaatgacag tcaggcgctg gtggaggcga
      841 tgaatgagct ctggcataac gatgaaaccg ccagccgcta tggcgaaaac tcgcgtcgtc
      901 gttttgaaga gatgtttact gccgaccata tgattgacgc ttacgtcaat ctctacacta
      961 cgctgctgga aagcaaatcc tgaggtcagc tacccccgcc gttgcggcgg gggttaacgc
     1021 gttagtgacg actcttcaga ttatcaatca cttccccgag atccagcccc tggtcctgca
     1081 gcagcaccag caggtggtac atcaaatccg acgcctcatt cttcagctcg aagcggtcgt
     1141 tcaccgtggc ggccagcgcg gtctccacgc cttcttcccc cactttctgc gcaatacgct
     1201 tggtgccgct ggcgtacagc ttcgccgtgt aggagctctc cgggtcggcg tgcttacgct
     1261 cggccagcag ctgctccagc tgatagagga acagccactg gtggccggtt tcgccgaagc
     1321 agctggtggt gcctttgtgg caggtcggcc cgatcgggtt caccagcacc agcagcgtat
     1381 cgttgtcaca atccggcgtg atgctcacta cgttgaggac attgcccgag gtttcaccct
     1441 tggtccacag ccgttgtttg gtgcgtgagt aaaaggtgac cttgccggtg gcttcggttt
     1501 tctccagcgc ctccttgttc atgtagccca gcatcagcac ttcgccggat acggcgtgct
     1561 gcacgatggc cggcatcatg ccgtccgttt tttcccagtc cagctgttca gtcaacatac
     1621 ccttatctcc acgccctgtg ccgccaggta cgctttcagt tcgccaatat taataatctg
     1681 tttgtggaat accgaggccg ccagcgcgcc gtcaacgttg gcgtcgcg