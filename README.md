# DNA alignment

## Requierements

- Node.js
- yarn (optional, npm can be used)

## Installing and running

```bash
# from root dir
cd api
yarn install & # don't forget to kill it later
yarn start
cd ../app
yarn install
yarn start
```

## Testing

Testing files to search by file are in root dir - `file[1..5]`
