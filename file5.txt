LOCUS       AB000176                 241 bp    DNA     linear   BCT 24-JUL-2016
DEFINITION  Escherichia coli DNA for mannosyl transferase, phosphoribosyl-ATP
            pyrophosphohydrolase:phosphoribosyl-AMP cyclohydrolase, partial
            cds.
ACCESSION   AB000176
VERSION     AB000176.1
KEYWORDS    phosphoribosyl-ATP pyrophosphohydrolase:phosphoribosyl-AMP
            cyclohydrolase; phosphoribosyl-ATP pyrophosphohydrolase;
            phosphoribosyl-AMP cycl ohydrolase; mannosyl transferase.
SOURCE      Escherichia coli
  ORGANISM  Escherichia coli
            Bacteria; Proteobacteria; Gammaproteobacteria; Enterobacterales;
            Enterobacteriaceae; Escherichia.
REFERENCE   1
  AUTHORS   Sugiyama,T., Kido,N., Kato,Y., Koide,N., Yoshida,T. and Yokochi,T.
  TITLE     Evolutionary relationship among rfb gene clusters synthesizing
            mannose homopolymer as O-specific polysaccharides in Escherichia
            coli and Klebsiella
  JOURNAL   Gene 198 (1-2), 111-113 (1997)
   PUBMED   9370271
REFERENCE   2  (bases 1 to 241)
  AUTHORS   Sugiyama,T.
  TITLE     Direct Submission
  JOURNAL   Submitted (27-DEC-1996) Tsuyoshi Sugiyama, Aichi Medical
            University, Department of Microbiology and Immunology; Yazako,
            Nagakute, Aichi 480-11, Japan (E-mail:sugiyama@aichi-med-u.ac.jp,
            Tel:0561-62-3311, Fax:0561-63-9187)
FEATURES             Location/Qualifiers
     source          1..241
                     /organism="Escherichia coli"
                     /mol_type="genomic DNA"
                     /strain="B1002 serotype O8"
                     /db_xref="taxon:562"
     gene            <1..60
                     /gene="wbdC"
     CDS             <1..60
                     /gene="wbdC"
                     /codon_start=1
                     /transl_table=11
                     /product="mannosyl transferase"
                     /protein_id="BAA23230.1"
                     /translation="DHMIDAYVNLYTTLLESKS"
     regulatory      70..90
                     /regulatory_class="terminator"
     gene            complement(99..>241)
                     /gene="hisI"
     CDS             complement(99..>241)
                     /gene="hisI"
                     /EC_number="3.5.4.19"
                     /codon_start=3
                     /transl_table=11
                     /product="phosphoribosyl-ATP
                     pyrophosphohydrolase:phosphoribosyl-AMP cyclohydrolase"
                     /protein_id="BAA23231.1"
                     /translation="TALAATVHDRFELTNEASDLMYHLLVLLQDQGLDLGEVIDNLKN
                     RH"
ORIGIN      
        1 gaccatatga ttgacgccta tgtcaatctc tacactacat tgctggaaag caaatcctga
       61 gagatgctac ccccgccgtt gctgcggggg ccaacgcgtt aatgccgatt cttcagatta
      121 tcaatcactt ctccgagatc cagcccttga tcctgcaaca acaccagcag gtgatacatc
      181 aaatcagatg cctcgttggt cagttcaaag cggtcatgta ctgtcgctgc cagtgcggtt
      241 t