import React from 'react'

import { createStyles, Theme, makeStyles } from '@material-ui/core/styles'

import Input from './components/Input'
import Sequence from './components/Sequence'

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			display: 'flex',
			flexDirection: 'column',
			alignItems: 'center',
			justifyContent: 'center',
			width: '100%',
			height: '100%'
		},
		result: {
			width: '100%',
			height: 0,
			transition: 'height 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
			overflow: 'hidden',
			'&[data-show="true"]': {
				height: '100%',
				overflow: 'auto'
			}
		},
		resultContent: {
			display: 'flex',
			flexDirection: 'column',
			justifyContent: 'center',
			alignItems: 'center',
			margin: theme.spacing(6, 3),
			'& > div > *': {
				marginBottom: theme.spacing(3)
			},
			'& > div > *:last-child': {
				marginBottom: 0
			}
		},
		loader: {
			alignSelf: 'center',
			justifySelf: 'center',
			margin: theme.spacing(2)
		}
	})
)

const mapSequence = (seq: Input, i: number) => <Sequence key={i} value={seq} />

type Input = {
	name: string
	match: [string, string]
}

const App: React.FC = () => {
	const classes = useStyles()
	const [sequences, setSequences] = React.useState<Input[]>([])
	const [isLoading, setLoading] = React.useState(false)

	const handleSetSequence = React.useCallback(
		(value: string, alignment: string, k: number) => {
			try {
				setLoading(true)
				fetch(
					`http://localhost:8080/find?value=${value}&alignment=${alignment}&k=${k}`
				)
					.then(res => res.json())
					.then(res => {
						setSequences(res as Input[])
						setLoading(false)
					})
			} catch (err) {
				console.log(err)
			}
		},
		[setLoading]
	)

	return (
		<div className={classes.root}>
			<Input onSearch={handleSetSequence} isLoading={isLoading} />
			<div data-show={sequences.length > 0} className={classes.result}>
				<div style={{ display: 'flex' }}>
					<div style={{ width: '100%' }}>
						<div className={classes.resultContent}>
							<div style={{ maxWidth: '100%' }}>
								{sequences.map(mapSequence)}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export default App
