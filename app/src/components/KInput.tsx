import * as React from 'react'

import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import Tooltip from '@material-ui/core/Tooltip'
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			height: 48,
			display: 'flex',
			alignItems: 'center',
			borderRadius: 24,
			padding: theme.spacing(0, 3)
		},
		input: {
			width: 64
		}
	})
)

interface Props {
	onSetK: (value: number) => void
}

const TextInput: React.FC<Props> = props => {
	const { onSetK } = props
	const classes = useStyles()
	const [value, setValue] = React.useState('20')

	const handleChange = React.useCallback(
		(event: React.ChangeEvent<HTMLInputElement>) => {
			const value = event.target.value
			setValue(value)
			onSetK(parseInt(value))
		},
		[setValue, onSetK]
	)

	return (
		<Paper className={classes.root} elevation={2}>
			<Tooltip title="K top results">
				<TextField
					value={value}
					onChange={handleChange}
					autoFocus
					type="number"
					InputProps={{ disableUnderline: true }}
					className={classes.input}
				/>
			</Tooltip>
		</Paper>
	)
}

export default TextInput
