import * as React from 'react'

import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import Tooltip from '@material-ui/core/Tooltip'
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			borderRadius: 24,
			backgroundColor: theme.palette.grey[800]
		},
		button: {
			height: 48,
			borderRadius: 0,
			padding: theme.spacing(1, 3),
			textTransform: 'unset',
			letterSpacing: '0.00938em',
			fontSize: '1rem',
			backgroundColor: theme.palette.grey[700],
			color: theme.palette.getContrastText(theme.palette.grey[700]),
			borderColor: theme.palette.grey[800],
			'&:hover': {
				backgroundColor: theme.palette.grey[800]
			},
			'&:first-child': {
				borderTopLeftRadius: 24,
				borderBottomLeftRadius: 24,
				borderRightWidth: 1,
				borderRightStyle: 'solid'
			},
			'&:last-child': {
				borderTopRightRadius: 24,
				borderBottomRightRadius: 24,
				borderLeftWidth: 1,
				borderLeftStyle: 'solid'
			},
			'&[data-active="true"]': {
				backgroundColor: theme.palette.primary.main,
				color: theme.palette.getContrastText(
					theme.palette.primary.main
				),
				borderColor: theme.palette.primary.dark,
				'&:hover': {
					backgroundColor: theme.palette.primary.dark
				}
			}
		}
	})
)

type Alignment = 'global' | 'local'

interface Props {
	onSetAlignment: (alignment: Alignment) => void
}

const Input: React.FC<Props> = props => {
	const { onSetAlignment } = props
	const classes = useStyles()
	const [alignment, setAlignment] = React.useState<Alignment>('global')

	const handleAlignmentChange = React.useCallback(
		(alignment: Alignment) => () => {
			setAlignment(alignment)
			onSetAlignment(alignment)
		},
		[setAlignment, onSetAlignment]
	)

	return (
		<Tooltip title="Alignment">
			<Paper className={classes.root} elevation={2}>
				<Button
					onClick={handleAlignmentChange('global')}
					data-active={alignment === 'global'}
					className={classes.button}
				>
					Global
				</Button>
				<Button
					onClick={handleAlignmentChange('local')}
					data-active={alignment === 'local'}
					className={classes.button}
				>
					Local
				</Button>
			</Paper>
		</Tooltip>
	)
}

export default Input
