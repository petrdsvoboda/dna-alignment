import * as React from 'react'
import clsx from 'clsx'

import Typography from '@material-ui/core/Typography'
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles'
import { orange, green, blue, pink } from '@material-ui/core/colors'

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			display: 'flex',
			width: theme.spacing(3),
			height: theme.spacing(3),
			alignItems: 'center',
			justifyContent: 'center',
			flexShrink: 0,
			'& > p': {
				// fontWeight: 500
				fontSize: '1.1rem'
			}
		},
		a: { backgroundColor: orange[200] },
		c: { backgroundColor: green[200] },
		g: { backgroundColor: blue[200] },
		t: { backgroundColor: pink[200] }
	})
)

interface Props {
	value: string
}

const Nucleic: React.FC<Props> = props => {
	const { value } = props
	const classes = useStyles()

	return (
		<div
			className={clsx(classes.root, {
				[classes.a]: value === 'a' || value === 'A',
				[classes.c]: value === 'c' || value === 'C',
				[classes.g]: value === 'g' || value === 'G',
				[classes.t]: value === 't' || value === 'T'
			})}
		>
			<Typography>{value.toUpperCase()}</Typography>
		</div>
	)
}

export default Nucleic
