import * as React from 'react'

import IconButton from '@material-ui/core/IconButton'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import Tooltip from '@material-ui/core/Tooltip'
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles'
import FileIcon from '@material-ui/icons/AttachFile'

import { parse } from '../parser'

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			display: 'flex',
			alignItems: 'center',
			borderRadius: 24
			// backgroundColor: theme.palette.grey[800]
		},
		input: {
			height: 48,
			padding: theme.spacing(0, 3),
			borderTopLeftRadius: 24,
			borderBottomLeftRadius: 24,
			display: 'flex',
			alignItems: 'center'
		},
		fileInput: {
			display: 'none'
		},
		button: {
			// backgroundColor: theme.palette.primary.main,
			// color: theme.palette.primary.contrastText,
			// '&:hover': {
			// 	backgroundColor: theme.palette.primary.dark
			// }
		}
	})
)

interface Props {
	onSetValue: (value: string) => void
}

const TextInput: React.FC<Props> = props => {
	const { onSetValue } = props
	const classes = useStyles()
	const [value, setValue] = React.useState('')
	const fileEl = React.useRef<HTMLInputElement>(null)

	const handleChange = React.useCallback(
		(event: React.ChangeEvent<HTMLInputElement>) => {
			const value = event.target.value.toUpperCase()

			if (value === '') setValue(value)

			if (['A', 'C', 'G', 'T'].includes(value[value.length - 1])) {
				setValue(value)
				onSetValue(value)
			}
		},
		[onSetValue, setValue]
	)

	const handleFileClick = React.useCallback(() => {
		if (!fileEl.current) return
		fileEl.current.click()
	}, [fileEl])

	const handleSearchFile = React.useCallback(
		(event: React.ChangeEvent<HTMLInputElement>) => {
			const files = event.target.files
			if (!files || !files[0]) return
			const file = files[0]
			const fr = new FileReader()
			fr.onloadend = (e: ProgressEvent<FileReader>) => {
				if (!e.target) return

				if (typeof e.target.result === 'string') {
					const value = parse(e.target.result).toUpperCase()
					setValue(value)
					onSetValue(value)
				}
			}
			fr.readAsText(file)
		},
		[onSetValue]
	)

	return (
		<Paper className={classes.root} elevation={2}>
			<div className={classes.input}>
				<TextField
					value={value}
					onChange={handleChange}
					autoFocus
					placeholder="sequence..."
					InputProps={{
						disableUnderline: true
					}}
				/>
			</div>
			<Tooltip title="Search">
				<div className={classes.root}>
					<input
						ref={fileEl}
						type="file"
						onChange={handleSearchFile}
						className={classes.fileInput}
					/>
					<IconButton
						color="primary"
						onClick={handleFileClick}
						className={classes.button}
					>
						<FileIcon />
					</IconButton>
				</div>
			</Tooltip>
		</Paper>
	)
}

export default TextInput
