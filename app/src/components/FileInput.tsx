import * as React from 'react'

import Button from '@material-ui/core/Button'
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles'
import FileIcon from '@material-ui/icons/Description'

import { parse } from '../parser'

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {},
		input: {
			display: 'none'
		},
		button: {
			height: 48,
			borderRadius: 24,
			textTransform: 'unset',
			letterSpacing: '0.00938em',
			fontSize: '1rem',
			'& svg': {
				marginRight: theme.spacing(1)
			}
		}
	})
)

interface Props {
	onSearch: (value: string) => void
}

const FileInput: React.FC<Props> = props => {
	const { onSearch } = props
	const classes = useStyles()
	const fileEl = React.useRef<HTMLInputElement>(null)

	const handleFileClick = React.useCallback(() => {
		if (!fileEl.current) return
		fileEl.current.click()
	}, [fileEl])

	const handleSearchFile = React.useCallback(
		(event: React.ChangeEvent<HTMLInputElement>) => {
			const files = event.target.files
			if (!files || !files[0]) return
			const file = files[0]
			const fr = new FileReader()
			fr.onloadend = (e: ProgressEvent<FileReader>) => {
				if (!e.target) return

				if (typeof e.target.result === 'string') {
					onSearch(parse(e.target.result))
				}
			}
			fr.readAsText(file)
		},
		[onSearch]
	)

	return (
		<div className={classes.root}>
			<input
				ref={fileEl}
				type="file"
				onChange={handleSearchFile}
				className={classes.input}
			/>
			<Button
				variant="contained"
				color="primary"
				onClick={handleFileClick}
				className={classes.button}
				size="large"
			>
				<FileIcon />
				Search using file
			</Button>
		</div>
	)
}

export default FileInput
