import * as React from 'react'

import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles'

import Nucleic from './Nucleic'

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			display: 'flex',
			alignItems: 'center'
		},
		name: {
			marginRight: theme.spacing(3),
			color: theme.palette.grey[500],
			fontSize: '1.5rem',
			letterSpacing: '0.1rem'
		},
		alignment: {
			overflow: 'hidden',
			backgroundColor: theme.palette.grey[800]
		},
		content: {
			overflowX: 'auto',
			overflowY: 'hidden'
		},
		row: {
			display: 'flex'
		}
	})
)

type Input = {
	name: string
	match: [string, string]
}

interface Props {
	value: Input
}

const mapNucleic = (nuc: string, i: number) => <Nucleic key={i} value={nuc} />

const Sequence: React.FC<Props> = props => {
	const { value } = props
	const classes = useStyles()

	return (
		<div className={classes.root}>
			<Typography className={classes.name}>{value.name}</Typography>
			<Paper className={classes.alignment} elevation={2}>
				<div className={classes.content}>
					<div className={classes.row}>
						{value.match[0].split('').map(mapNucleic)}
					</div>
					<div className={classes.row}>
						{value.match[1].split('').map(mapNucleic)}
					</div>
				</div>
			</Paper>
		</div>
	)
}

export default Sequence
