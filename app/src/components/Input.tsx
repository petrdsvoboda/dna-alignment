import * as React from 'react'

import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import Typography from '@material-ui/core/Typography'
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles'
import SearchIcon from '@material-ui/icons/Search'

import AlignmentInput from './AlignmentInput'
import KInput from './KInput'
import TextInput from './TextInput'

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			width: '100%',
			height: 96,
			display: 'flex',
			alignItems: 'center',
			boxShadow:
				'0px 3px 1px -2px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 1px 5px 0px rgba(0,0,0,0.12)',
			justifyContent: 'center',
			backgroundColor: theme.palette.grey[800]
		},
		input: {
			display: 'flex',
			alignItems: 'center',
			'& > *': {
				marginRight: theme.spacing(2)
			},
			'& > *:last-child': {
				marginRight: theme.spacing(0)
			}
		},
		inputOr: {
			fontStyle: 'italic',
			color: theme.palette.primary.contrastText
		},
		text: {
			display: 'flex'
		},
		textInput: {
			padding: theme.spacing(1, 3),
			marginRight: theme.spacing(3)
		},
		button: {
			height: 48,
			borderRadius: 24,
			textTransform: 'unset',
			letterSpacing: '0.00938em',
			fontSize: '1rem',
			marginLeft: theme.spacing(6),
			'& > span > *:first-child': {
				marginRight: theme.spacing(1)
			}
		},
		fileInput: {
			display: 'none'
		}
	})
)

type Alignment = 'global' | 'local'

interface Props {
	onSearch: (value: string, alignment: string, k: number) => void
	isLoading: boolean
}

const Input: React.FC<Props> = props => {
	const { onSearch, isLoading } = props
	const classes = useStyles()
	const [sequence, setSequence] = React.useState('')
	const [alignment, setAlignment] = React.useState<Alignment>('global')
	const [k, setK] = React.useState(20)

	const handleSequenceChange = React.useCallback(
		(value: string) => {
			setSequence(value)
		},
		[setSequence]
	)

	const handleAlignmentChange = React.useCallback(
		(alignment: Alignment) => {
			setAlignment(alignment)
		},
		[setAlignment]
	)

	const handleKChange = React.useCallback(
		(k: number) => {
			setK(k)
		},
		[setK]
	)

	const handleSearch = React.useCallback(() => {
		if (sequence === '') return
		onSearch(sequence, alignment, k)
	}, [sequence, alignment, k, onSearch])

	return (
		<div className={classes.root}>
			<div className={classes.input}>
				<Typography className={classes.inputOr}>Find</Typography>
				<TextInput onSetValue={handleSequenceChange} />
				<Typography className={classes.inputOr}>using</Typography>
				<AlignmentInput onSetAlignment={handleAlignmentChange} />
				<Typography className={classes.inputOr}>getting</Typography>
				<KInput onSetK={handleKChange} />
				<Button
					variant="contained"
					color="primary"
					onClick={handleSearch}
					className={classes.button}
				>
					{isLoading ? (
						<CircularProgress color="inherit" size={24} />
					) : (
						<SearchIcon />
					)}
					{isLoading ? 'Searching' : 'Search'}
				</Button>
			</div>
		</div>
	)
}

export default Input
