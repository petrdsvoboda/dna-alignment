const isOriginRow = (row: string): boolean => row.slice(0, 6) === 'ORIGIN'

const parseDNA = (row: string): string =>
	row
		.split(' ')
		.slice(1)
		.join('')

const parseSequence = (data: string): string => {
	const sequence =
		data
			.split('\n')
			.map(r => r.trimLeft())
			.reduce<undefined | string>(
				(acc, curr) =>
					acc === undefined
						? isOriginRow(curr)
							? ''
							: undefined
						: acc + parseDNA(curr),
				undefined
			) || ''

	return sequence
}

const parse = (file: string): string => {
	return parseSequence(file)
}

export { parse }
