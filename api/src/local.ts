import {
	FindFn,
	Match,
	Instance as BaseInstance,
	MatchFn,
	Arrow,
	Result
} from './types'

type Instance = {
	bestCell: {
		score: number
		coords: [number, number]
	}
} & BaseInstance

const repeat = (callback: (index: number) => void) => (n: number): void => {
	for (let i = 0; i < n; i++) {
		callback(i)
	}
}

const buildGrid = <T>(x: number) => (y: number): T[][] =>
	Array.from(Array(x), () => new Array(y))

function runLin(callback: (x: number, y: number) => void) {
	return (array: any[][]): any => {
		const xLenght = array.length
		const yLength = array[0].length

		repeat((y: number) => {
			repeat((x: number) => {
				callback(x, y)
			})(xLenght)
		})(yLength)
	}
}

const getScore = (a: string) => (b: string): 1 | -1 =>
	a.toLowerCase() === b.toLowerCase() ? 1 : -1
const getLength = (s: string): number => s.length

const calculateCell = (instance: Instance) => (x: number, y: number): void => {
	if (x === 0 || y === 0) return

	const compareScore = getScore(instance.target[x - 1])(
		instance.sequence[y - 1]
	)

	const diagScore = instance.grid[x - 1][y - 1] + compareScore
	let topScore = -1
	repeat(i => {
		const index = x - 1 - i
		const score = instance.grid[index][y] - 2 * (i + 1)
		if (score > topScore) {
			topScore = score
		}
	})(x - 1)
	let leftScore = -1
	repeat(i => {
		const index = y - 1 - i
		const score = instance.grid[x][index] - 2 * (i + 1)
		if (score > leftScore) {
			leftScore = score
		}
	})(y - 1)

	const maxScore = Math.max(
		diagScore,
		Math.max(topScore, Math.max(leftScore, 0))
	)
	instance.grid[x][y] = maxScore

	if (maxScore > instance.bestCell.score) {
		instance.bestCell = {
			score: maxScore,
			coords: [x - 1, y - 1]
		}
	}

	let arrow: Arrow
	if (maxScore === diagScore) arrow = 'd'
	else if (maxScore === topScore) arrow = 'l'
	else arrow = 't'
	instance.arrows[x - 1][y - 1] = arrow
}

const match: MatchFn<Instance> = instance => {
	const target = instance.target
	const sequence = instance.sequence

	instance.grid[0][0] = 0
	repeat(i => {
		instance.grid[0][i] = 0
	})(getLength(sequence + 1))
	repeat(i => {
		instance.grid[i][0] = 0
	})(getLength(target + 1))

	runLin(calculateCell(instance))(instance.grid)

	let [x, y] = instance.bestCell.coords
	const bestValue = instance.grid[x + 1][y + 1]
	let tmpTarget: string[] = []
	let tmpSequence: string[] = []
	while (true) {
		const value = instance.grid[x + 1][y + 1]
		if (value === 0) break
		const arrow = instance.arrows[x][y]
		if (arrow === 'd') {
			tmpTarget = [target[x], ...tmpTarget]
			tmpSequence = [sequence[y], ...tmpSequence]
			x -= 1
			y -= 1
		} else if (arrow === 't') {
			tmpTarget = ['-', ...tmpTarget]
			tmpSequence = [sequence[y], ...tmpSequence]
			y -= 1
		} else {
			tmpTarget = [target[x], ...tmpTarget]
			tmpSequence = ['-', ...tmpSequence]
			x -= 1
		}
	}

	return {
		match: [tmpTarget.join(''), tmpSequence.join('')],
		name: instance.name,
		value: bestValue
	}
}

const find: FindFn = target => (data): Result[] => {
	return data.map(input =>
		match({
			name: input.name,
			target: target,
			sequence: input.sequence,
			grid: buildGrid<number>(getLength(target + 1))(
				getLength(input.sequence + 1)
			),
			arrows: buildGrid<Arrow>(getLength(target))(
				getLength(input.sequence)
			),
			bestCell: {
				score: -1,
				coords: [-1, -1]
			}
		})
	)
}

export { find }
