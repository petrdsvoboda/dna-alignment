import * as fs from 'fs'
import * as path from 'path'

const FOLDER = path.join(__dirname, 'data')

const loadFile = (fileName: string): string => {
	const filePath = path.join(FOLDER, fileName)
	return fs.readFileSync(filePath, 'utf-8')
}

export { loadFile }
