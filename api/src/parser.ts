import { Input } from './types'

const MAX_SEQUENCES = 100

const isLocusRow = (row: string): boolean => row.slice(0, 5) === 'LOCUS'
const isOriginRow = (row: string): boolean => row.slice(0, 6) === 'ORIGIN'

const parseDNA = (row: string): string =>
	row
		.split(' ')
		.slice(1)
		.join('')

const parseSequence = (data: string): Input => {
	const sequence = data
		.split('\n')
		.map(r => r.trimLeft())
		.reduce<Input & { parse: boolean }>(
			(acc, curr) => {
				if (isLocusRow(curr)) {
					const name = curr
						.slice(6)
						.trimLeft()
						.split(' ')[0]
					return {
						...acc,
						name
					}
				} else if (isOriginRow(curr)) {
					return {
						...acc,
						parse: true
					}
				} else if (acc.parse) {
					return {
						...acc,
						sequence: acc.sequence + parseDNA(curr)
					}
				}

				return acc
			},
			{ sequence: '', name: '', parse: false }
		)

	return sequence
}

const parse = (file: string): Input[] => {
	const sequences = file
		.split('//')
		.slice(0, MAX_SEQUENCES)
		.map(parseSequence)

	return sequences
}

export { parse }
