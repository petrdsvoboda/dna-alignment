import { FindFn, Match, Instance, MatchFn, Arrow, Result } from './types'

const repeat = (callback: (index: number) => void) => (n: number): void => {
	for (let i = 0; i < n; i++) {
		callback(i)
	}
}

const buildGrid = <T>(x: number) => (y: number): T[][] =>
	Array.from(Array(x), () => new Array(y))

function runDiag(callback: (x: number, y: number) => void) {
	return (array: any[][]): any => {
		const xLenght = array.length
		const yLength = array[0].length
		const maxLength = Math.max(xLenght, yLength)

		repeat((i: number) => {
			repeat((j: number) => {
				const y = yLength - j - 1
				const x = i - y
				if (x >= 0 && x < xLenght) {
					callback(y, x)
				}
			})(yLength)
		})(2 * maxLength)
	}
}

const getScore = (a: string) => (b: string): 1 | -1 =>
	a.toLowerCase() === b.toLowerCase() ? 1 : -1
const getLength = (s: string): number => s.length

const calculateCell = (instance: Instance) => (x: number, y: number): void => {
	if (
		x === 0 ||
		y === 0 ||
		x > instance.target.length ||
		y > instance.sequence.length
	)
		return

	const a = instance.target
	const b = instance.sequence

	const diagScore = instance.grid[x - 1][y - 1] + getScore(a[x - 1])(b[y - 1])
	const topScore = instance.grid[x - 1][y] - 1
	const leftScore = instance.grid[x][y - 1] - 1

	const maxScore = Math.max(diagScore, Math.max(topScore, leftScore))
	instance.grid[x][y] = maxScore

	let arrow: Arrow
	if (maxScore === diagScore) arrow = 'd'
	else if (maxScore === topScore) arrow = 'l'
	else arrow = 't'
	instance.arrows[x - 1][y - 1] = arrow
}

const match: MatchFn<Instance> = instance => {
	const target = instance.target
	const sequence = instance.sequence

	instance.grid[0][0] = 0

	let score = 0
	repeat(i => {
		instance.grid[0][i] = score--
	})(getLength(sequence + 1))
	score = 0
	repeat(i => {
		instance.grid[i][0] = score--
	})(getLength(target + 1))

	runDiag(calculateCell(instance))(instance.grid)

	let x = target.length - 1
	let y = sequence.length - 1
	let tmpTarget: string[] = []
	let tmpSequence: string[] = []
	while (x !== -1 && y !== -1) {
		const arrow = instance.arrows[x][y]
		if (arrow === 'd') {
			tmpTarget = [target[x], ...tmpTarget]
			tmpSequence = [sequence[y], ...tmpSequence]
			x -= 1
			y -= 1
		} else if (arrow === 't') {
			tmpTarget = ['-', ...tmpTarget]
			tmpSequence = [sequence[y], ...tmpSequence]
			y -= 1
		} else {
			tmpTarget = [target[x], ...tmpTarget]
			tmpSequence = ['-', ...tmpSequence]
			x -= 1
		}
	}

	const value = tmpTarget.reduce(
		(acc, _, index) => acc + getScore(tmpTarget[index])(tmpSequence[index]),
		0
	)

	return {
		match: [tmpTarget.join(''), tmpSequence.join('')],
		name: instance.name,
		value
	}
}

const find: FindFn = target => (data): Result[] => {
	return data.map(input =>
		match({
			name: input.name,
			target: target,
			sequence: input.sequence,
			grid: buildGrid<number>(getLength(target + 1))(
				getLength(input.sequence + 1)
			),
			arrows: buildGrid<Arrow>(getLength(target))(
				getLength(input.sequence)
			)
		})
	)
}

export { find }
