export type Input = {
	sequence: string
	name: string
}

export type Instance = {
	name: string
	target: string
	sequence: string
	grid: Grid
	arrows: Arrows
}
export type Match = [string, string]
export enum MatchTuple {
	Target = 0,
	Sequence = 1
}

export type Result = {
	match: Match
	name: string
	value: number
}

export type MatchFn<T extends Instance> = (instance: T) => Result
export type FindFn = (value: string) => (data: Input[]) => Result[]

export type Grid = number[][]

export type Arrow = 't' | 'd' | 'l'
export type Arrows = Arrow[][]
export type Stack<T> = T[]

export const stackInit = <T>(): Stack<T> => []
export const stackPush = <T>(value: T) => (stack: Stack<T>): Stack<T> => {
	stack.push(value)
	return stack
}
export const stackPop = <T>(stack: Stack<T>): Stack<T> => {
	stack.pop()
	return stack
}
