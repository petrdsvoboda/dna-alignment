import * as express from 'express'
import * as bodyParser from 'body-parser'

import * as cors from 'cors'
import * as logger from 'morgan'

import { sort } from '@petrdorg/utils/lib/array'

import { find as findGlobal } from './global'
import { find as findLocal } from './local'
import { FindFn } from './types'
import { loadFile } from './loader'
import { parse } from './parser'

const MAX_LENGTH = 250

export interface ServerOptions {
	port?: number
	env: string
}

const sequences = parse(loadFile('gbbct1.seq')).map(v => ({
	...v,
	sequence: v.sequence.slice(0, MAX_LENGTH)
}))

console.log('Starting server...')
const app = express()

if (process.env.mode !== 'production') {
	app.use(logger('dev'))
}

app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use('/find', (req, res) => {
	const { value, alignment, k } = req.query as {
		value: string
		alignment: 'global' | 'local'
		k: string
	}
	const find: FindFn = alignment === 'global' ? findGlobal : findLocal
	let result = find(value.slice(0, MAX_LENGTH))(sequences)

	const kVal = parseInt(k)
	result = sort(result, { ascending: false }, 'value').slice(0, kVal)

	res.send(result)
})

const port = process.env.port || 8080
app.listen(port, () => {
	console.log('Server listening on port: ', port)
})
